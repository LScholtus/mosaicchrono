##    (2nd) 3rd - 7th September 2024

### Handling and processing chronological data in archaeology

Chronological research produces different types of chronological data. The relevance of the dates obtained depends very much on the sample being dated and the uncertainty of the date, which can vary greatly depending on the method used. Dating methods therefore provide heterogeneous data, making it difficult for archaeologists to reconstruct the sequence of events studied.  
The aim of this summer school is to discuss how this data should be recorded, managed and processed in order to trace the history of the objects, sites and communities studied by archaeologists. The main dating methods will be presented, along with software solutions for linking them together and modelling the succession of archaeological events. This will also be an opportunity to discuss good practice in choosing dating techniques and presenting the results obtained.

### Organisers

Lizzie Scholtus, Léonard Dumont, Oliver Nakoinz

### Contact

lizzie.scholtus@ufg.uni-kiel.de  
Leonard.Dumont@u-bourgogne.fr

{{<figure src="/logos.png">}}
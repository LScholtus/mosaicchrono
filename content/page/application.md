---
title: Application
subtitle: Application form to MOSAICdata
comments: false
---

Applications are open to researchers and young researchers (doctoral and post-doctoral), as well as to all archaeology professionals. 
Although an interest in data processing and analysis is expected from participants, a thorough knowledge of the methodology or software is not required.  

Accepted applicants will have to pay a fee of **50€** to pay for their accommodation.

To apply to this summerschool please fill in [this application form](/form_v2.pdf) and send it to : 
[lizzie.scholtus@ufg.uni-kiel.de](lizzie.scholtus@ufg.uni-kiel.de) **AND** [Leonard.Dumont@u-bourgogne.fr](Leonard.Dumont@u-bourgogne.fr)


[Download the application form](/form_v2.pdf) *(Wait a few seconds to fill the form in the Adobe software or use your browser to open it)*

Please send this form **before April 30th**
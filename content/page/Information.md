---
title: General Information
subtitle: Venue, Accommodation, Fees, Travel
comments: false
---

# Venue

## Bibracte EPCC

[Bibracte website](https://www.bibracte.fr/letablissement-public-bibracte-epcc)

The summerschool will be held at the Bibracte European Center, in France:  

BIBRACTE EPCC - Centre archéologique européen  
37 rue des Trois sommets  
58370 Glux-en-Glenne  
France  

{{<figure src="/bibracte_vue.jpg" caption="Bibracte research center (© Bibracte)">}}

# Accommodation

Accommodation will be provided in dormitories at the European Center for all participants.

# Fees

Participants have to pay a fee of **50€** which will cover accomodation and food for the complete week in Bibracte EPCC.  

# Travel Information

Transport costs are not covered. However, a shuttle bus will be provided to take participants to the Bibracte European Centre from **Le Creusot TGV** or **Étang-sur-Arroux** stations on Sunday 1st and Monday 2nd of September.

{{<figure src="/map.jpg" caption="Location of Bibracte and the train stations">}}
  

More information on how to come to Bibracte by car can be found [here](https://www.bibracte.fr/en/useful-information). The research center is located in Glux-en-Glenn.
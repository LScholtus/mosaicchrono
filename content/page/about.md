---
title: About MOSAICchrono
subtitle: International Research School
comments: false
---

## Handling and processing chronological data in archaeology

Chronological research produces different types of chronological data. These fall into two main categories: relative, enabling events to be placed in relation to one another, and absolute, enabling a calendar date to be assigned to events. Relative chronological data is based on the stratigraphic relationships between archaeological events, as well as on so-called closed contexts, containing objects that were abandoned simultaneously and are therefore contemporary. This is the basis for identifying different chrono-cultural horizons and defining different relative chronological systems that vary from one region to another. Absolute dates, on the other hand, are obtained using different methods from disciplines such as biology, physics or chemistry, such as dendrochronology or radiocarbon dating. The relevance of the dates thus obtained depends very much on the sample being dated and the uncertainty of the date, which can vary greatly depending on the method used. Dating methods therefore provide heterogeneous data, making it difficult for archaeologists to reconstruct the sequence of events studied.  

The aim of this summer school is to discuss how this data should be recorded, managed and processed in order to trace the history of the objects, sites and communities studied by archaeologists. The main dating methods will be presented, along with software solutions for linking them together and modelling the succession of archaeological events. This will also be an opportunity to discuss good practice in choosing dating techniques and presenting the results obtained.  

This summer school will last six days, during which several data and chronological management solutions will be illustrated and implemented using the Rstudio software. Following theoretical background and presentations of existing workflows, the afternoon workshops are dedicated to practical examples provided by our lecturers or the participants.



## Organisation

### Institutes

- Université de Bourgogne
- Université Franco-Allemande / Deutsch-Französische Hochschule
- Institute of Pre- and Protohistoric Archaeology - Kiel University
- Johanna Mestorf Academy - Kiel University
- Bibracte EPCC

### Organisation Committee

- Lizzie Scholtus, Dr., Institute of Pre- and Protohistoric Archaeology, Kiel University, Germany
- Léonard Dumont, Dr., UMR 6298 ARTEHIS, University of Bourgogne, France - Ghent University, Department of Archaeology - Belgium
- Oliver Nakoinz, Prof. Dr., Lecturer, Institute of Pre- and Protohistoric Archaeology, Kiel University, Germany


### Guest Lecturers

- John Collis, Prof., Emeritus, Department of Archaeology, University of Sheffield
- Enrico Crema, Associate Professor, Department of Archaeology, University of Cambridge
- Sébastien Durost, Bibracte research center
- Eythan Levy, Postdoctoral researcher, Instut für Judaistik, University of Bern
- Sébastien Plutniak, Chargé de recherche CNRS, University of Tours
- Ian Roalkvam, Doctoral Research Fellow, University of Oslo, Department of Archaeology, Conservation and History
- Joe Roe, Projektmitarbeiter, Institut für Archäologische Wissenschaften, University of Bern


{{<figure src="/logos.png">}}

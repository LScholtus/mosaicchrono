---
title: Information
subtitle: Fees
comments: false
---

Participants have to pay a fee of --€ which will cover accomodation and food for the complete week in Bibracte EPCC.  

Transport costs are not covered. However, a shuttle bus will be provided to take participants to the Bibracte European Centre from Le Creusot TGV or Étang-sur-Arroux stations.

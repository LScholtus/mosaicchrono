---
title: MOSAICchrono Program
subtitle: International Research School
comments: false
---

# Summerschool program

This summerschool will last five days during which several chronological systems and processing methods, in different fields, will be illustrated and implemented using the Rstudio software.

The Introduction to R, on the first day, is an optional module dedicated to participant with no previous knowledge on the scripting language R.  

Provisional program

![Program](/program.png)

# Social events

{{<figure src="/bibracte_site.jpg" caption="View at the top of Mont Beuvray (© L. Scholtus)">}}


## Visit of Bibracte archaeological site and museum

A guided tour of the archaeological site (Mont Beuvray) and of the museum will be provided to all participants.

{{<figure src="/bibracte_musee.jpg" caption="Bibracte museum (© Bibracte)">}}

## Gallic dinner

The participants will also enjoy a gallic dinner at the restaurant [Le Chaudron](https://www.bibracte.fr/en/restaurant-le-chaudron)
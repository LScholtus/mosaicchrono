---
title: Blocks
subtitle: Presentations
comments: false
---

## Block 0: Introduction to R

- [Block 0, part 1](/MOSAICdata_block0_1.html)
- [Block 0, part 2](/MOSAICdata_block0_2.html)
- [Block 0, part 3](/MOSAICdata_block0_3.html)
     - [Block 0, part 3 - Solutions](/MOSAICdata_block0_3_solution.html)
- [John Collis Introduction](/Collis_intro.pdf)

## Block 1: Data Management

- [Block 1, part 1](/Part1.html)
- [Block 1, part 2](/MOSAICchrono_block1_2.html)
    - [Block 1, part 2 - Exercises](/MOSAICchrono_block1_4.html)
    - [Block 1, part 2 - Solutions](/MOSAICchrono_block1_4s.html)


## Block 2: Dating Methods

- [Block 2, Shoreline dating](/roalkvam_mosaic.pdf)
    - [Block 2, Shoreline dating - Workshop](/shoredate_workshop.html)
- [Block 2, Dendrochronology](/Dendro.pdf)

## Block 3: Radiocarbon Data

- [Block 3, C14 - Aoristic](/mosaic_c14.html)

## Block 4: Software Application

- [Block 4, ChronoLog](/handout.zip)
- [Block 4, Archeoviz](/plutniak_mosaic-archeoviz.pdf)

## Scripts

- [PeriodO](/mosaicchrono-periodo.R)
- [Regex](/mosaicchrono-regex.R)
- [Xronos](/mosaicchrono-xronos.R)
---
title: Information
subtitle: Venue
comments: false
---

## Bibracte EPCC

The summerschool will be held at the Bibracte European Center, France:  

BIBRACTE EPCC - Centre archéologique européen  
37 rue des Trois sommets  
58370 Glux-en-Glenne  
France  

[Bibracte website](https://www.bibracte.fr/letablissement-public-bibracte-epcc)

![](/bibracte_vue.jpg)

